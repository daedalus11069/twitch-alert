This script hooks into twitch webhooks in order to post updates to mirc/irc channels when a followed user goes live. The standard rate limits apply when using this script according to the [twitch limits](https://dev.twitch.tv/docs/api/guide/#rate-limits), for 'Bearer token is not provided'.

Requirements:

-   It is recommended to use a dynamic dns service like [No-IP](https://www.noip.com/).
    -   An http://IP url may be used as well, if desired, however the above is more recommended than this.
-   Reachable UPnP-enabled router
-   Twitch account
    -   2FA enabled.
    -   Application registered with [twitch dev console](https://dev.twitch.tv/console).

Of note, this script uses Universal Plug-n-Play functionality to expose a port on the router the requisite computer is connected to in order to listen for twitch events. So if that's a deal breaker don't proceed any further.

## Installation

To obtain a client id, go to the [twitch dev console](https://dev.twitch.tv/console) and create an application. Url can be `http://localhost`, and doesn't matter aside from that.

To use the script, it must be installed of course; [download](https://bitbucket.org/daedalus11069/twitch-alert/get/master.zip) the files and extract them into their own folder in mirc's scripts directory. This directory can typically be found by running `//echo -ag $scriptdir`(both slashes are required); make sure the script has its own folder, with a name such as `TwitchAlert`. After the script is in place, load it by going to mirc's scripts editor, then click the `File` menu and go to `load`. Navigate to the `twitch-notifier.mrc` file and load it. (Re)initialization isn't required for this script to work, and will only post a message if it is enabled.

The next steps are not automatic, so the following command must be run:

`/install_twitch clientId callbackUrl port`

Requried parameters:

-   `clientId`: Must be the id supplied by the dev console. This is not 'client secret', but it should be kept secret.
-   `callbackUrl`: Must be a url with the http or https protocol.
-   `port`: Must be the port that is going to be used by the script to listen for twitch events. Any number greater or equal to 8000 works

Example usage of install command:

`/install_twitch 1a2b3c4d5 8085`

Further help: `/help_twitch install`

The first command will tell you what to do further, provided the required params are given, which is as follows:

-   `/follow_users_twitch`: Can be ran anywhere, but must be ran with a space-separated list of usernames you want to follow.
    -   Example: `/follow_users_twitch user1 user2 user3`
    -   To unfollow users, the command is `/unfollow_users_twitch user1 user2 user3`;
    -   Further help: `/help_twitch follow`
-   `/add_channel_twitch`: Must be ran in the channel on the server you want to notify.
    -   To remove a notified channel, the command is `/rem_channel_twitch` in the channel on the server you want to stop notifying.
    -   Further help: `/help_twitch notify`

## Other usage

If the script needs to be disabled without purging the user follows, run `/disable_twitch`. Likewise, to re-enable, run `/enable_twitch`.

## Further information

If for whatever reason anything needs to be debugged, add `debug=$true` to `[general]` in `config.ini`.

Lastly, if a manual unsubscribe is required, run `/unsubscribe_twitch` to unfollow all users. This command should not be used for unfollowing unless a purge is required.

## License

Copyright (c) 2019 Brendan Strohmeier-Parmer

There are no plans to license this code; do not distribute without permission.
