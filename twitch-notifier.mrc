alias install_twitch {
    if ($2 == $null) {
        echo 4 -ga Invalid parameters supplied.  Proper command usage(without quotes): "/install_twitch clientid callbackUrl"
        echo 4 -ga Further help(without quotes): "/help_twitch install"
    }
    else {
        var %filename = $qt($scriptdir $+ config.ini)
        var %client_id = $twitch_config(auth,client_id)
        var %callback = $twitch_config(auth,callback)
        var %port = $twitch_config(auth,port)
        if ((%client_id == $null) || (%callback == $null) || (%port == $null)) {
            if (($2 != $null) && ($3 >= 8000)) {
                writeini %filename auth client_id $1
                echo 3 -ga Client ID set.
                writeini %filename auth callback $2
                echo 3 -ga Callback url set to $2.
                writeini %filename auth port $3
                echo 3 -ga Port set to $3.
                writeini %filename auth secret $rand_str(16)
                echo 3 -ga Please run the command(without quotes) "/follow_users_twitch users to follow" you would like to follow the status of.
                echo 3 -ga Further help(without quotes): "/help_twitch follow"
                echo 3 -ga Add channels to notify users in by using(without quotes): "/add_channel_twitch" in the channel you want to add.
                echo 3 -ga Further help(without quotes): "/help_twitch notify"
            }
        }
    }
}

alias -l rand_str {
    if (!$isid) {
        return
    }
    var %len = $1
    var %len_i = 1
    var %str = $null
    while (%len_i <= %len) {
        if ($rand(1,2) == 1) {
            var %randChar = $rand(0,9)
        }
        else {
            if ($rand(1,2) == 1) {
                var %randChar = $rand(A,Z)
            }
            else {
                var %randChar = $rand(a,z)
            }
        }
        var %str = %str $+ %randChar
        inc %len_i 1
    }
    return %str
}

alias -l get_scon {
    var %i = 1 | var %num = $scon(0)
    var %scon = 1
    while (%i <= %num) {
        if ($scon(%i) == $cid) {
            var %scon = %i
            break
        }
        inc %i 1
    }
    return %scon
}

alias follow_users_twitch {
    var %filename = $qt($scriptdir $+ config.ini)
    var %i = 1 | var %num = $numtok($$1-, 32)
    while (%i <= %num) {
        var %user = $gettok($$1-, %i, 32)
        var %followed_tracked = $twitch_config(followed_internal, %user)
        if (%followed_tracked == $null) {
            writeini %filename followed_internal %user $false
        }
        inc %i 1
    }
    stop_twitch
    sockopen -e twitch_alert.follow api.twitch.tv 443
    sockmark twitch_alert.follow $$1-
    echo 3 -ga Following users: $join_tokens_twitch($chr(44) $+ $chr(160), $$1-, 32) ...
}

alias unfollow_users_twitch {
    sockopen -e twitch_alert.unfollow api.twitch.tv 443
    sockmark twitch_alert.unfollow $$1-
    echo 3 -ga Unfollowing users: $join_tokens_twitch($chr(44) $+ $chr(160), $$1-, 32) ...
}

alias add_channel_twitch {
    var %filename = $qt($scriptdir $+ config.ini)
    var %existing_channels = $readini(%filename, notify_channels, scon_ $+ $get_scon)
    writeini %filename notify_channels scon_ $+ $get_scon $addtok(%existing_channels, $chan, 44)
    echo 3 -ga $chan added on connection number $get_scon $+ .
}

alias rem_channel_twitch {
    var %filename = $qt($scriptdir $+ config.ini)
    var %existing_channels = $readini(%filename, notify_channels, scon_ $+ $get_scon)
    var %remaining_channels = $remtok(%existing_channels, $chan, 1, 44)
    if (%remaining_channels == $null) {
        remini %filename notify_channels scon_ $+ $get_scon
    }
    else {
        writeini %filename notify_channels scon_ $+ $get_scon %remaining_channels
    }
    echo 3 -ga $chan removed on connection number $get_scon $+ .
}

alias -l listen_twitch {
    socklisten -p twitch_alert.webhook $twitch_config(auth,port)
}

alias -l stop_twitch {
    sockclose twitch_alert.*
}

alias -l stop_and_listen_twitch {
    stop_twitch
    listen_twitch
}

alias -l subscribe_twitch {
    stop_twitch
    sockopen -e twitch_alert.subscribe api.twitch.tv 443
    sockmark twitch_alert.subscribe $iif($1 !== $null,$1,$false)
}

alias unsubscribe_twitch {
    stop_twitch
    sockopen -e twitch_alert.unsubscribe api.twitch.tv 443
}

alias enable_twitch {
    writeini %filename general disabled $false
    echo 3 -ga Twitch Alert enabled.
}

alias disable_twitch {
    writeini %filename general disabled $true
    echo 3 -ga Twitch Alert disabled.
}

alias help_twitch {
    var %file = $qt($scriptdir $+ help.ini)
    var %topic = $1
    if (%topic == $null) {
        var %topic = topics
    }
    echo -ga -
    echo -ga Usage: $readini(%file,%topic,usage)
    echo -ga Example: $readini(%file,%topic,example)
    echo -ga Description: $readini(%file,%topic,description)
}

alias -l join_tokens_twitch {
    if (!$isid) {
        return
    }
    var %glue = $1
    var %arr = $2
    var %c = $3
    var %str = $null
    var %i = 1 | var %num = $numtok(%arr, %c)
    while (%i <= %num) {
        if (%i == 1) {
            var %gluestr = $null
        }
        else {
            var %gluestr = %glue
        }
        var %str = %str $+ %gluestr $+ $gettok(%arr, %i, %c)
        inc %i 1
    }
    return %str
}

alias twitch_debug {
    var %filename = $qt($scriptdir $+ config.ini)
    if ($1 == on) {
        writeini %filename general debug $true
    }
    elseif ($1 == off) {
        writeini %filename general debug $false
    }
}

on *:LOAD: {
    echo 3 -ga Twitch Notifier loaded.  Please run "/help_twitch install" without quotes for installation instructions.
}

on *:SOCKOPEN:twitch_alert.follow: {
    var %users = $sock($sockname).mark
    var %headers = $+(Client-ID: ,$twitch_config(auth,client_id))
    write_request $sockname GET /helix/users $build_query_array(login,%users) application/x-www-form-urlencoded %headers
}

on *:SOCKREAD:twitch_alert.follow: {
    if ($sockerr > 0) {
        echo 4 -ga Error: $sock($sockname).wsmsg
        goto error
    }
    var %read = $false
    var %users_followed = $false
    :nextread
    sockread %data
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        elseif ((%read) && (%users_followed == $false)) {
            sockread -f %data
            var %num_users = $numtok($sock($sockname).mark, 32)
            var %i = 0
            while ((%i < %num_users) && (%num_users <= 100)) {
                var %username = $json(%data,data,%i,login)
                var %userid = $json(%data,data,%i,id)
                add_watched_user %username %userid
                inc %i 1
            }
            sockclose $sockname
            subscribe_twitch $true
            var %users_followed = $true
        }
    }
    :error
}

on *:SOCKOPEN:twitch_alert.unfollow: {
    var %users = $sock($sockname).mark
    var %filename = $qt($scriptdir $+ config.ini)
    var %user_ids = $null | var %user_i = 1 | var %user_num = $ini(%filename, followed, 0)
    while ((%user_i <= %user_num) && (%user_i <= 100)) {
        var %user = $ini(%filename, followed, %user_i)
        if ($istok(%users, %user, 32)) {
            var %user_ids = %uses $readini(%filename, followed, %user)
        }
        inc %user_i 1
    }
    var %user_id_query = $build_query_array(user_id,%user_ids)
    var %headers = $+(Client-ID:,$chr(32),$twitch_config(auth,client_id))
    if (%user_id_query != $null) {
        var %json = $+($chr(123),"hub.callback":",$twitch_config(auth,callback),:,$twitch_config(auth,port),$&
            ",$chr(44),"hub.mode":"unsubscribe",$chr(44),"hub.topic":"https://api.twitch.tv/helix/streams?,$&
            %user_id_query,",$chr(44),"hub.lease_seconds":1,$chr(44),"hub.secret":",$twitch_config(auth,secret),$&
            ",$chr(125))
        write_request $sockname POST /helix/webhooks/hub %json application/json %headers
    }
}

on *:SOCKREAD:twitch_alert.unfollow: {
    if ($sockerr > 0) {
        echo 4 -ga Error: $sock($sockname).wsmsg
        goto error
    }
    var %filename = $qt($scriptdir $+ config.ini)
    var %read = $false
    :nextread
    sockread %data
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        elseif (%read) {
            sockread -f %data
            var %users = $sock($sockname).mark
            var %num_users = $numtok(%users, 32)
            var %i = 1
            while ((%i <= %num_users) && (%num_users <= 100)) {
                var %username = $gettok(%users, %i, 32)
                rem_watched_user %username
                inc %i 1
            }
            echo 3 -ga Users: $join_tokens_twitch($chr(44) $+ $chr(160), $sock($sockname).mark, 32) have been unfollowed.
        }
    }
    :error
}

alias -l add_watched_user {
    var %file = $qt($scriptdir $+ config.ini)
    writeini %file followed $$1 $$2
}

alias -l rem_watched_user {
    var %file = $qt($scriptdir $+ config.ini)
    remini %file followed $$1
    remini %file followed_internal $$1
}

on *:SOCKLISTEN:twitch_alert.webhook: {
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    sockaccept $+($sockname,_api)
}

on *:SOCKREAD:twitch_alert.webhook_api: {
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    var %read = $false
    var %hook_handled = $false
    :nextread
    sockread %data
    if ($twitch_config(general,debug) == $true) {
        window -e @twich_alert_debug -1 -1 1000 500
        aline -p @twich_alert_debug $asctime([HH:nn:ss]) $+ : $+ %data
    }
    if ($regex(signature, %data, /X-Hub-Signature: (sha256=[\w\-]+)/i) == 1) {
        sockmark $sockname $regml(signature, 1)
    }
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        if ((%read) && (%hook_handled == $false)) {
            write_response $sockname
            sockread -f %data
            if ($verify_hash(%data, $sock($sockname).mark)) {
                var %reduced_data = $chr(91)
                var %i = 0 | var %num = $json(%data, data, length)
                while (%i < %num) {
                    var %reduced_data = %reduced_data $+ $+($chr(123), $+ $&
                        "user_name":,",$json(%data, data, %i, user_name),",$chr(44), $+ $&
                        "title":,",$json(%data, data, %i, title),",$chr(44), $+ $&
                        "game_id":,",$json(%data, data, %i, game_id)," $+ $&
                        $chr(125))
                    if (%i < $calc(%num - 1)) {
                        var %reduced_data = %reduced_data $+ $chr(44)
                    }
                    inc %i 1
                }
                var %reduced_data = %reduced_data $+ $chr(93)
                get_game_names %reduced_data
                var %hook_handled = $true
            }
        }
    }
}

alias -l get_game_names {
    var %json = $1-
    sockopen -e twitch_alert.get_game_names api.twitch.tv 443
    sockmark twitch_alert.get_game_names %json
}

on *:SOCKOPEN:twitch_alert.get_game_names: {
    var %ids = $null | var %json = $sock($sockname).mark
    var %i = 0 | var %num = $json(%json, length)
    while (%i < %num) {
        var %ids = %ids $json(%json, %i, game_id)
        inc %i 1
    }
    var %headers = $+(Client-ID:,$chr(32),$twitch_config(auth,client_id))
    write_request $sockname GET /helix/games $build_query_array(id, %ids) text/plain %headers
}

on *:SOCKREAD:twitch_alert.get_game_names: {
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    var %read = $false
    var %api_hit = $false
    :nextread
    sockread %data
    if ($twitch_config(general,debug) == $true) {
        window -e @twich_alert_debug -1 -1 1000 500
        aline -p @twich_alert_debug $asctime([HH:nn:ss]) $+ : $+ %data
    }
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        elseif ((%read) && (%api_hit == $false)) {
            sockread -f %data
            var %reduced_data = $sock($sockname).mark
            var %i = 0 | var %num = $json(%data, data, length)
            while (%i < %num) {
                var %gameid = $json(%data, data, %i, id)
                var %gamename = $json(%data, data, %i, name)
                var %reduced_data = $replace_game(%gameid, %gamename, %reduced_data)
                inc %i 1
            }
            process_twitch_channel_notifications 0 %reduced_data
            var %api_hit = $true
        }
    }
}

alias -l replace_game {
    if (!$isid) {
        return
    }
    var %id = $1
    var %name = $2
    var %json = $3
    return $regsubex(%json, /("game_id":\s?" $+ %id $+ ")/i, "game":" $+ %name $+ ")
}

alias -l verify_hash {
    if (!$isid) {
        return
    }
    var %text = $1
    var %hash = $2
    return $iif(sha256= $+ $hmac(%text, $twitch_config(auth,secret), sha256) === %hash,$true,$false)
}

on *:SOCKWRITE:twitch_alert.webhook_api: {
    if ($sock($sockname).sq == 0) {
        sockclose $sockname
        stop_and_listen_twitch
    }
}

alias -l twitch_config {
    return $readini($qt($scriptdir $+ config.ini), $1, $2)
}

alias -l theme {
    if (!$isid) {
        return
    }
    var %filename = $qt($scriptdir $+ config.ini)
    var %json = $1
    var %theme = $2
    var %message_type = $3
    var %message_template = $readini(%filename, theme_ $+ %theme, %message_type)
    var %param_i = 1 | var %param_num = $regex(theme, %message_template, /\{(\w+):?(\w*)\}/gi)
    var %message = %message_template
    while (%param_i <= %param_num) {
        var %token = $regmlex(theme, %param_i, 1)
        var %filter = $regmlex(theme, %param_i, 2)
        var %replacement = $json(%json, %token)
        if (%filter != $null) {
            var %replacement = $lower(%replacement)
        }
        var %message = $regsubex(%message, /\{ $+ %token $+ (:\w*)?\}/i, %replacement)
        inc %param_i 1
    }
    return %message
}

alias -l process_twitch_channel_notifications {
    var %channel_i = $1
    var %channel_data = $2-

    var %json = $json(%channel_data, %channel_i, user_name)
    notify_twitch $theme(%json, $twitch_config(general, theme), live)

    if ($calc(%channel_i + 1) < $json(%channel_data, length)) {
        .timer 1 5 /process_twitch_channel_notifications $calc(%channel_i + 1) %channel_data
    }
}

alias -l notify_twitch {
    var %message = $1-
    var %filename = $qt($scriptdir $+ config.ini)
    var %scon_i = 1 | var %scon_num = $ini(%filename, notify_channels, 0)
    while (%scon_i <= %scon_num) {
        var %scon = $ini(%filename, notify_channels, %scon_i)
        process_twitch_notifications %scon $readini(%filename, notify_channels, %scon) %message
        inc %scon_i 1
    }
}

alias -l process_twitch_notifications {
    var %scon = $1
    var %channels = $2
    var %message = $3-
    var %scon_num = $gettok(%scon, 2, 95)
    if ($twitch_config(general,disabled) != $true) {
        scon -st1 %scon_num /msg $gettok(%channels, 1, 44) %message
    }
    if ($gettok(%channels, 2-, 44) !== $null) {
      .timer 1 5 /process_twitch_notifications %scon $gettok(%channels, 2-, 44) %message
    }
}

on $*:SOCKOPEN:/^twitch_alert\.((un)?subscribe)$/i: {
    var %mode = $regml(1)
    var %filename = $qt($scriptdir $+ config.ini)
    var %user_ids = $null | var %user_i = 1 | var %user_num = $ini(%filename, followed, 0)
    while ((%user_i <= %user_num) && (%user_i <= 100)) {
        var %user = $ini(%filename, followed, %user_i)
        if (%mode == subscribe) {
            var %user_followed = $twitch_config(followed_internal,%user)
            if ((%user_followed == $false) || (%user_followed == $null)) {
                var %user_ids = %user_ids $readini(%filename, followed, %user)
            }
        }
        elseif (%mode == unsubscribe) {
            if ($twitch_config(followed_internal,%user) == $true) {
                var %user_ids = %user_ids $readini(%filename, followed, %user)
            }
        }
        inc %user_i 1
    }
    var %user_id_query = $build_query_array(user_id,%user_ids)
    var %headers = $+(Client-ID:,$chr(32),$twitch_config(auth,client_id))
    if (%user_id_query != $null) {
        var %json = $+($chr(123),"hub.callback":",$twitch_config(auth,callback),:,$twitch_config(auth,port),$&
            ",$chr(44),"hub.mode":",%mode,",$chr(44),"hub.topic":"https://api.twitch.tv/helix/streams?,$&
            %user_id_query,",$chr(44),"hub.lease_seconds":864000,$chr(44),"hub.secret":",$twitch_config(auth,secret),$&
            ",$chr(125))
        ;864000 is the normal time
        write_request $sockname POST /helix/webhooks/hub %json application/json %headers
    }
}

on $*:SOCKREAD:/^twitch_alert\.((un)?subscribe)$/i: {
    var %mode = $regml(1)
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    var %read = $false
    var %listened = $false
    :nextread
    sockread %data
    if ($twitch_config(general,debug) == $true) {
        window -e @twich_alert_debug -1 -1 1000 500
        aline -p @twich_alert_debug $asctime([HH:nn:ss]) $+ : $+ %data
    }
    if ($gettok(%data, 2-3, 32) == 202 Accepted) {
        sockmark $sockname $true $sock($sockname).mark
    }
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        elseif ((%read) && (%listened == $false)) {
            var %mark = $sock($sockname).mark
            sockclose $sockname
            if ($gettok(%mark, 1, 32) == $true) {
                socklisten -p $+(twitch_alert.,%mode,_challenge) $twitch_config(auth,port)
                sockmark $+(twitch_alert.,%mode,_challenge) $gettok(%mark, 2, 32)
            }
            var %listened = $true
        }
    }
}

on $*:SOCKLISTEN:/^twitch_alert\.((un)?subscribe)_challenge$/i: {
    var %mode = $regml(1)
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    sockaccept $+(twitch_alert.,%mode,_api_challenge)
    sockmark $+(twitch_alert.,%mode,_api_challenge) $sock($sockname).mark
}

on $*:SOCKREAD:/^twitch_alert\.((un)?subscribe)_api_challenge$/i: {
    if ($sockerr > 0) {
        echo 4 -ga $sock($sockname).wsmsg
    }
    var %read = $false
    var %challenge_read = $false
    var %challenge_sent = $false
    :nextread
    sockread %data
    if ($twitch_config(general,debug) == $true) {
        window -e @twich_alert_debug -1 -1 1000 500
        aline -p @twich_alert_debug $asctime([HH:nn:ss]) $+ : $+ %data
    }
    if (($regex(challenge, $gettok(%data, 2-, 32),/\/\?hub\.challenge=(.*?)&.*/i) == 1) && (%challenge_read == $false)) {
        var %challenge = $regml(challenge, 1)
        sockmark $sockname %challenge $sock($sockname).mark
        var %challenge_read = $true
    }
    if (%data == $null) {
        if (%read == $false) {
            var %read = $true
            goto nextread
        }
        elseif ((%read) && (%challenge_sent == $false)) {
            var %challenge = $gettok($sock($sockname).mark, 1, 32)
            sockmark $sockname $gettok($sock($sockname).mark, 2, 32)
            write_response $sockname Content-Type: text/plain~Content-Length: $len(%challenge)
            sockwrite $sockname %challenge
            var %challenge_sent = $true
        }
    }
}

on $*:SOCKWRITE:/^twitch_alert\.((un)?subscribe)_api_challenge$/i: {
    var %mode = $regml(1)
    var %filename = $qt($scriptdir $+ config.ini)
    var %users = $null
    var %user_i = 1 | var %user_num = $ini(%filename, followed, 0)
    while ((%user_i <= %user_num) && (%user_i <= 100)) {
        var %user = $ini(%filename, followed, %user_i)
        if (%mode == subscribe) {
            writeini %filename followed_internal %user $true
        }
        elseif (%mode == unsubscribe) {
            writeini %filename followed_internal %user $false
        }
        var %users = %users %user
        inc %user_i 1
    }
    if ($sock($sockname).mark == $true) {
        var %mode_pretty = $iif(%mode == subscribe,followed,unfollowed)
        echo 3 -ga Users: $join_tokens_twitch($chr(44) $+ $chr(160), %users, 32) have been %mode_pretty $+ .
    }
    if ($sock($sockname).sq == 0) {
        sockclose $sockname
        sockclose $+(twitch_alert.,%mode,_challenge)
        if (%mode == subscribe) {
            .timertwitch_sub_refresh off
            .timertwitch_sub_refresh -o 1 863995 /subscribe_twitch
            listen_twitch
        }
    }
}

on $*:SOCKCLOSE:/^twitch_alert\.((un)?subscribe).*/i: {
   ;echo -ag closed $sockname
}

alias -l sockwrite {
    if ($twitch_config(general,debug) == $true) {
        window -e @twich_alert_debug_sockwrite -1 -1 1000 500
        aline -p @twich_alert_debug_sockwrite $asctime([HH:nn:ss]) $+ : $+ $1-
    }
    !sockwrite $1-
}

alias -l write_response {
    var %sockname = $$1
    var %headers = $2-
    sockwrite -n %sockname HTTP/1.1 200 OK
    sockwrite -n %sockname Server: mIRC/ $+ $version (Windows $os $+ ; en)
    sockwrite -n %sockname Host: $+($twitch_config(auth,callback),:,$twitch_config(auth,port))
    var %header_i = 1 | var %header_num = $numtok(%headers, 126)
    while (%header_i <= %header_num) {
        var %header = $gettok(%headers, %header_i, 126)
        sockwrite -n %sockname %header
        inc %header_i 1
    }
    sockwrite -n %sockname
}

alias -l write_request {
    var %sockname = $$1
    var %method = $$2
    var %endpoint = $$3
    var %query = $$4
    var %contentType = $$5
    var %headers = $$6-
    var %len = $len(%query)

    sockwrite -n %sockname %method %endpoint $+ $iif(%method == GET,? $+ %query) HTTP/1.1
    sockwrite -n %sockname Server: mIRC/ $+ $version (Windows $os $+ ; en)
    sockwrite -n %sockname Host: $sock($sockname).addr
    var %i = 1 | var %num = $numtok(%headers, 126)
    while (%i <= %num) {
        var %header = $gettok(%headers, %i, 126)
        var %name = $gettok(%header, 1, 58)
        var %value = $gettok(%header, 2, 58)
        sockwrite -n %sockname $+(%name,: ,%value)
        inc %i 1
    }
    sockwrite -n %sockname Content-Type: %contentType
    if (%method == POST) {
        sockwrite -n %sockname Content-Length: %len
    }
    sockwrite -n %sockname
    if (%method == POST) {
        sockwrite %sockname %query
    }
}

alias -l build_query_array {
    if ($isid) {
        var %key = $$1
        var %values = $2
        var %return = $null
        var %i = 1 | var %num = $numtok(%values, 32)
        while (%i <= %num) {
            var %value = $gettok(%values, %i, 32)
            var %and = $iif(%i > 1,&)
            var %return = $+(%return,%and,%key,=,%value)
            inc %i 1
        }
        return %return
    }
}

alias json {
    var %c = jsonidentifier,%x = 2,%str,%p,%v
    if (!$com(%c)) {
        .comopen %c MSScriptControl.ScriptControl
        var %JSON = $qt($scriptdir $+ JSON.js)
        bread %JSON 0 $file(%JSON).size &jscript
        noop $com(%c,language,4,bstr,jscript)
        noop $com(%c,addcode,1,&bstr,&jscript)
        noop $com(%c,addcode,1,bstr,function getValue $chr(40) $+ json $+ $chr(41) $chr(123) if $chr(40) $+ typeof json === 'object' $+ $chr(41) $chr(123) return JSON.stringify $+ $chr(40) $+ json $+ $chr(41) $+ ; $chr(125) else $chr(123) return json; $chr(125) $chr(125) $+ ;)
        ;get the list of parameters
        while (%x <= $0) {
            %p = $($+($,%x),2)
            if (%p == $null) { noop }
            elseif (%p isnum || $qt($noqt(%p)) == %p) { %str = $+(%str,[,%p,]) }
            else { %str = $+(%str,[,",%p,",]) }
            inc %x
        }
        ;get data from inputted json
        noop $com(%c,eval,1,bstr,$+(getValue,$chr(40),$escapeJSON($1),%str,$chr(41)))
        var %ret = $com(%c).result
        .comclose %c
        return %ret
    }
    else {
        .comclose %c
    }
}

alias -l escapeJSON {
    if (!$isid) return
    return $regsubex($1,/(')/g,\ $+ \1)
}
